CRATE_NAME = $(notdir $(CURDIR))

all: build build-web pages icons

icons:
	# copy icons
	cp -a /home/ciro/TOOL-ADD/open_tool-add/resources/pattern-lab/source/icons ./target/web/icons


pages:
	./scripts/build_pages.sh

build: ~/.cargo/bin/wasm-pack ~/.cargo/bin/rustup
	wasm-pack build

build-web: www build-sass	
	cd www && npm install && npm run build-crate -- $(CRATE_NAME) && npm install

build-sass:
	echo "building sass files"
	./scripts/build_sass_files.sh

serve-pages:
	cd www && npm run test-server

serve-web: www
	cd www && npm run start

mocks: install-mockrs
	mockrs gen ./mocks/task_template.json --output ./mocks/tasks.json
	# mockrs serve ./mocks/tasks.json
	json-server ./mocks/tasks.json --port 9000

clean:
	rm -rf ./www ./pkg

test-all: test-rust test-web test-js

test-rust: pkg www 
	cargo test

test-web: pkg www
	wasm-pack test --firefox --headless

test-js: pkg www
	cd www && npm run test:unit && npm run test:e2e

install-rust:
ifeq ($(OS), Windows_NT)
	echo "Install Rust by downloading https://static.rust-lang.org/rustup/rustup-init.sh"
else
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
endif

install-wasm-pack:
ifeq ($(OS), Windows_NT)
	echo "Install Wasm-Pack by downloading https://github.com/rustwasm/wasm-pack/releases/download/v0.8.1/wasm-pack-init.exe"
else
	curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
endif

install-cargo-edit: ~/.cargo/bin/cargo-add

install-mockrs: ~/.cargo/bin/mockrs

install-json-server:
	npm install -g json-server

~/.cargo/bin/mockrs: ~/.cargo/bin/rustup
	cargo install mockrs --git https://github.com/PrivateRookie/mockrs.git

www:
	git clone https://gitlab.com/opentooladd/wasm-app-boilerplate www && rm -rf www/.git

install-node-sass: 
	npm install -g node-sass

~/.cargo/bin/wasm-pack:
	make install-wasm-pack

~/.cargo/bin/rustup:
	make install-rust

~/.cargo/bin/cargo-add: ~/.cargo/bin/rustup
	cargo install cargo-edit

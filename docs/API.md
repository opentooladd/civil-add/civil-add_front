# API

Ce fichier référence les routes utilisées par ce projet, ainsi que les schémas de donnée entrant et sortant des requêtes REST.

## Schemas

### TaskStatus

```
enum TaskStatus {
  Brouillon,
  Publiee,
  Realisee,
  Archive,
}
```

### Task

```
{
  "id": String,
  "title": String,
  "body": String,
  "status": TaskStatus,
  "user_id": String,
  "contributor_id": Option<String>,
}
```

## GET `/tasks`
> Retourne toutes les tâches.
> **return**: Array<[Task](#Task)>

## GET `/task/{id}`
> Retourne une tâche par id.
> **return**: [Task](#Task)

## GET `/user/{user_id}/tasks`
> Retourne toutes les tâches d'un utilisateurs.
> **return**: Array<[Task](#Task)>

## DELETE `/task/{id}`
> Supprime une tâche.
> **return**:

## PUT `/task/{id}`
> Modifie une tâche.
> **body**: [Task](#Task)
> **return**: [Task](#Task)


use std::collections::HashMap;

use wasm_component::mustache;
use wasm_component::state_machine::Machine;
use crate::components::task_create::state::{
   State,
   States,
   CreateTask,
};
use wasm_bindgen::JsCast;
use crate::components::task_create::events::Events;
use crate::components::task_create::failures::Failures;
use wasm_component::v2::driver::dom::Driver;
use wasm_component::v2::driver::dom::{ log };

use crate::components::task_create::Components;

use wasm_bindgen_futures::spawn_local;

pub struct TaskCreate {
    state: States,
    pub components: HashMap<String, Driver<Components>>,
}

mod service;

impl Machine for TaskCreate {
   type Events = Events;
   type State = State;
   type States = States;
   type Failures = Failures;

   fn new() -> Self {
        let state = States::default();
        TaskCreate {
            state: state.clone(),
            components: HashMap::new(),
        }
    }

   fn transition(&mut self, event: Events) -> Result<&States, Failures> {
       match (self.get_state(), event) {
           (_, Events::CreateTaskError(error)) => {
               let mut n_state = self.get_raw_state().clone();
               n_state.error = Some(error);
               self.state = States::Error(n_state);
               self.run()
           }
           (_, Events::CreatedTask(id)) => {
              log(format!("created task: {}", id).as_str());
              web_sys::window().expect("could not get window")
                 .location().set_href("/home").unwrap();
              self.run()
           }
           (_, Events::CreateTask) => {
              let s = self.get_raw_state().clone();
              let window = web_sys::window().expect("could not get window");
              let document = window.document().expect("could not get document");
              let app = document.query_selector(".civil-add-front__app").unwrap().unwrap();
              let shadow = app.shadow_root().unwrap();

              let title_element = shadow.query_selector("#task-create__title").unwrap().unwrap();
              let title_element = title_element.dyn_into::<web_sys::HtmlInputElement>().unwrap();
              let title_value = title_element.value();
              let body_element = shadow.query_selector("#task-create__body").unwrap().unwrap();
              let body_element = body_element.dyn_into::<web_sys::HtmlTextAreaElement>().unwrap();
              let body_value = body_element.value();

              let c_task = CreateTask {
                 title: Some(title_value),
                 body: Some(body_value),
                 status: s.status,
                 author_id: Some("Ciro DE CARO".to_string()),
                 contributor_id: s.contributor_id,
              };
              spawn_local(service::create_task(c_task));
              self.run()
           }
       }
   }  
   
   fn run(&mut self) -> Result<&States, Failures> {
      match self.state.clone() {
         States::Error(e) => {
            Err(Failures::Message(e.error.unwrap()))
         }
         _ => {
            // log(format!("{:?}", self.get_state()).as_str());
            Ok(self.get_state())
         }
      }
   }

   fn get_state (&self) -> &States { 
      &self.state
   }

   fn get_state_mut(&mut self) -> &mut States {
       &mut self.state
   } 
    
   fn get_raw_state(&self) -> &State {
        match self.state {
           States::Ready(ref c)
           | States::Error(ref c) => c
        }
   }

   fn get_raw_state_mut(&mut self) -> &mut State {
        match self.state {
            States::Ready(ref mut c) 
            | States::Error(ref mut c) => c
        }
   }

   // optional but permit to use more functionnality of mustache
   // like lambdas and to react to other combination
   fn get_data(&mut self) -> mustache::MapBuilder {
       match self.state.clone() {
           States::Ready(s) => {
               let data = mustache::MapBuilder::new().insert("state", &s).unwrap().insert_bool("loading", false);
               data
           }
           States::Error(s) => {
               let data = mustache::MapBuilder::new().insert("state", &s).unwrap().insert_bool("loading", false);
               data
           }
       }
   }
}

use std::hash::Hash;
use std::cmp::Eq;
use serde::{ Serialize, Deserialize };

use crate::components::task_manager::state::{ TaskStatus };

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq)]
pub struct CreateTask {
   pub title: Option<String>,
   pub body: Option<String>,
   pub status: TaskStatus,
   pub author_id: Option<String>,
   pub contributor_id: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq)]
pub struct State {
   pub title: Option<String>,
   pub body: Option<String>,
   pub status: TaskStatus,
   pub author_id: Option<String>,
   pub contributor_id: Option<String>,
   pub error: Option<String>
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq)]
pub enum States {
   Ready(State),
   Error(State),
}

impl Default for States {
    fn default() -> Self {
        States::Ready(State {
           title: None,
           body: None,
           status: TaskStatus::Brouillon,
           author_id: None,
           contributor_id: None,
           error: None,
        })
    }
}

use serde::{ Serialize, Deserialize };

use crate::components::task_manager::state::Task;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Eq, Hash)]
pub enum Events {
   FetchTask(i32),
   GetTask(Task),
   GetTaskError(String),
   UpdateTask, 
   UpdateTaskError(String),
   UpdatedTask(String),
}

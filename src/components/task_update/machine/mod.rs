use std::collections::HashMap;

use wasm_component::mustache;
use wasm_component::state_machine::Machine;
use crate::components::task_update::state::{
   State,
   States,
   UpdateTask,
};
use wasm_bindgen::JsCast;
use crate::components::task_update::events::Events;
use crate::components::task_update::failures::Failures;
use wasm_component::v2::driver::dom::Driver;
use wasm_component::v2::driver::dom::{ log };

use crate::components::task_update::Components;

use wasm_bindgen_futures::spawn_local;

pub struct TaskUpdate {
    state: States,
    pub components: HashMap<String, Driver<Components>>,
}

mod service;

impl Machine for TaskUpdate {
   type Events = Events;
   type State = State;
   type States = States;
   type Failures = Failures;

   fn new() -> Self {
        let state = States::default();
        TaskUpdate {
            state: state.clone(),
            components: HashMap::new(),
        }
    }

   fn transition(&mut self, event: Events) -> Result<&States, Failures> {
       match (self.get_state(), event) {
           (_, Events::FetchTask(id)) => {
               self.state = States::Loading(self.get_raw_state().clone());
               spawn_local(service::get_task(id));
               self.run()
           }
           (_, Events::GetTask(task)) => {
               let n_state = State {
                  id: Some(task.id),
                  title: Some(task.title),
                  body: Some(task.body),
                  author_id: Some(task.author_id),
                  contributor_id: Some(task.contributor_id),
                  status: task.status,
                  error: None,
               };
               let window = web_sys::window().expect("could not get window");
               let document = window.document().expect("could not get document");
               let app = document.query_selector(".civil-add-front__app").unwrap().unwrap();
               let shadow = app.shadow_root().unwrap();

               let title_element = shadow.query_selector("#task-update__title").unwrap().unwrap();
               let title_element = title_element.dyn_into::<web_sys::HtmlInputElement>().unwrap();
               title_element.set_value(n_state.title.as_ref().unwrap());

               let body_element = shadow.query_selector("#task-update__body").unwrap().unwrap();
               let body_element = body_element.dyn_into::<web_sys::HtmlTextAreaElement>().unwrap();
               body_element.set_value(n_state.body.as_ref().unwrap());
               self.state = States::Ready(n_state);
               self.run()
           }
           (_, Events::UpdateTaskError(error)) | (_, Events::GetTaskError(error)) => {
               let mut n_state = self.get_raw_state().clone();
               n_state.error = Some(error);
               self.state = States::Error(n_state);
               self.run()
           }
           (_, Events::UpdatedTask(id)) => {
              log(format!("updated task: {}", id).as_str());
              web_sys::window().expect("could not get window")
                 .location().set_href("/home").unwrap();
              self.run()
           }
           (_, Events::UpdateTask) => {
              let s = self.get_raw_state().clone();
              let window = web_sys::window().expect("could not get window");
              let document = window.document().expect("could not get document");
              let app = document.query_selector(".civil-add-front__app").unwrap().unwrap();
              let shadow = app.shadow_root().unwrap();

              let title_element = shadow.query_selector("#task-update__title").unwrap().unwrap();
              let title_element = title_element.dyn_into::<web_sys::HtmlInputElement>().unwrap();
              let title_value = title_element.value();
              let body_element = shadow.query_selector("#task-update__body").unwrap().unwrap();
              let body_element = body_element.dyn_into::<web_sys::HtmlTextAreaElement>().unwrap();
              let body_value = body_element.value();

              let c_task = UpdateTask {
                 id: self.get_raw_state().id.clone().unwrap(),
                 title: Some(title_value),
                 body: Some(body_value),
                 status: s.status,
                 author_id: s.author_id,
                 contributor_id: s.contributor_id,
              };
              spawn_local(service::update_task(c_task));
              self.run()
           }
       }
   }  
   
   fn run(&mut self) -> Result<&States, Failures> {
      match self.state.clone() {
         States::Error(e) => {
            Err(Failures::Message(e.error.unwrap()))
         }
         _ => {
            // log(format!("{:?}", self.get_state()).as_str());
            Ok(self.get_state())
         }
      }
   }

   fn get_state (&self) -> &States { 
      &self.state
   }

   fn get_state_mut(&mut self) -> &mut States {
       &mut self.state
   } 
    
   fn get_raw_state(&self) -> &State {
        match self.state {
           States::Ready(ref c)
           | States::Loading(ref c)
           | States::Error(ref c) => c
        }
   }

   fn get_raw_state_mut(&mut self) -> &mut State {
        match self.state {
            States::Ready(ref mut c) 
            | States::Loading(ref mut c)
            | States::Error(ref mut c) => c
        }
   }

   // optional but permit to use more functionnality of mustache
   // like lambdas and to react to other combination
   fn get_data(&mut self) -> mustache::MapBuilder {
       match self.state.clone() {
           States::Ready(s) => {
               let data = mustache::MapBuilder::new().insert("state", &s).unwrap().insert_bool("loading", false);
               data
           }
           States::Loading(s) => {
               let data = mustache::MapBuilder::new().insert("state", &s).unwrap().insert_bool("loading", true);
               data
           }
           States::Error(s) => {
               let data = mustache::MapBuilder::new().insert("state", &s).unwrap().insert_bool("loading", false);
               data
           }
       }
   }
}

use std::collections::HashMap;

use wasm_component::mustache;
use wasm_component::state_machine::Machine;
use crate::components::task_manager::state::{
   State,
   TaskFilters,
   States,
};
use crate::components::task_manager::events::Events;
use crate::components::task_manager::failures::Failures;
use wasm_component::v2::driver::dom::Driver;

use crate::components::task_manager::Components;

use wasm_bindgen_futures::spawn_local;

pub struct TaskManager {
    state: States,
    pub components: HashMap<String, Driver<Components>>,
}

mod service;

impl Machine for TaskManager {
   type Events = Events;
   type State = State;
   type States = States;
   type Failures = Failures;

   fn new() -> Self {
        let state = States::default();
        TaskManager {
            state: state.clone(),
            components: HashMap::new(),
        }
    }

   fn transition(&mut self, event: Events) -> Result<&States, Failures> {
       match (self.get_state(), event) {
           (_, Events::OpenFilter) => {
              self.get_raw_state_mut().filter_opened = true;
              self.run()
           }
           (_, Events::CloseFilter) => {
              self.get_raw_state_mut().filter_opened = false;
              self.run()
           }
           (_, Events::SelectFilter(filter)) => {
              self.get_raw_state_mut().tasks = vec![];
              self.get_raw_state_mut().selected_task_filter = filter.clone();
              self.get_raw_state_mut().filter_opened = false;
              match filter {
                 TaskFilters::All => self.transition(Events::FetchTasks),
                 TaskFilters::Mine => self.transition(Events::FetchMyTasks),
                 TaskFilters::Contrib => self.transition(Events::FetchMyContrib)
              }
           }
           (_, Events::FetchMyTasks) => {
               self.state = States::Loading(self.get_raw_state().clone());
               spawn_local(service::get_my_tasks());
               self.run()
           }
           (_, Events::FetchMyContrib) => {
               self.state = States::Loading(self.get_raw_state().clone());
               spawn_local(service::get_my_contrib());
               self.run()
           }
           (_, Events::RedirectToCreateTask) => {
              let window = web_sys::window().expect("could not get window");
              window.location().set_href("/task").expect("could not redirect to task");
              self.run()
           }
           (_, Events::RedirectToUpdateTask(id)) => {
              let window = web_sys::window().expect("could not get window");
              window.location().set_href(format!("/task?id={}", id).as_str()).expect("could not redirect to task");
              self.run()
           }
           (_, Events::FetchTasks) => {
               self.state = States::Loading(self.get_raw_state().clone());
               spawn_local(service::get_tasks());
               self.run()
           }
           (_, Events::GetTasks(tasks)) => {
               self.state = States::Ready(State {
                  tasks,
                  selected_task_filter: self.get_raw_state().selected_task_filter.clone(),
                  filter_opened: false,
                  error: None
               });
               self.run()
           }
           (_, Events::GetTasksError(error)) | (_, Events::DeleteTaskError(error)) | (_, Events::UpdateTaskError(error)) => {
               self.state = States::Error(State {
                  tasks: self.get_raw_state().tasks.clone(),
                  selected_task_filter: self.get_raw_state().selected_task_filter.clone(),
                  filter_opened: false,
                  error: Some(error)
               });
               self.run()
           }
           (_, Events::SendDeleteTask(id)) => {
               // self.state = States::Loading(self.get_raw_state().clone());
               spawn_local(service::delete_task(id.clone()));
               self.run()
           }
           (_, Events::SendUpdateTask(id)) => {
               // self.state = States::Loading(self.get_raw_state().clone());
               let mut task = self.get_raw_state().tasks.iter().find(|t| t.id == id).unwrap().clone();
               task.title = "test_title".to_string();
               spawn_local(service::update_task(task.clone()));
               self.run()
           }
           (_, Events::DeleteTask(_id)) | (_, Events::UpdateTask(_id)) => {
               self.state = States::Loading(State {
                  tasks: vec![],
                  selected_task_filter: self.get_raw_state().selected_task_filter.clone(),
                  filter_opened: false,
                  error: None
               });
               self.transition(Events::FetchTasks)
           }
       }
   }  
   
   fn run(&mut self) -> Result<&States, Failures> {
      match self.state.clone() {
         States::Error(e) => {
            Err(Failures::Message(e.error.unwrap()))
         }
         _ => {
            // log(format!("{:?}", self.get_state()).as_str());
            Ok(self.get_state())
         }
      }
   }

   fn get_state (&self) -> &States { 
      &self.state
   }

   fn get_state_mut(&mut self) -> &mut States {
       &mut self.state
   } 
    
   fn get_raw_state(&self) -> &State {
        match self.state {
           States::Loading(ref c)
           | States::Ready(ref c)
           | States::Error(ref c) => c
        }
   }

   fn get_raw_state_mut(&mut self) -> &mut State {
        match self.state {
            States::Loading(ref mut c)
            | States::Ready(ref mut c) 
            | States::Error(ref mut c) => c
        }
   }

   // optional but permit to use more functionnality of mustache
   // like lambdas and to react to other combination
   fn get_data(&mut self) -> mustache::MapBuilder {
       match self.state.clone() {
           States::Loading(s) => {
               let data = mustache::MapBuilder::new().insert("state", &s).unwrap()
                  .insert_bool("isAllTasks", s.selected_task_filter == TaskFilters::All)
                  .insert_bool("isMyTasks", s.selected_task_filter == TaskFilters::Mine)
                  .insert_bool("isMyContrib", s.selected_task_filter == TaskFilters::Contrib)
                  .insert_bool("isOpen", s.filter_opened)
                  .insert_bool("loading", true);
               data
           }
           States::Ready(s) => {
               let data = mustache::MapBuilder::new().insert("state", &s).unwrap()
                  .insert_bool("isAllTasks", s.selected_task_filter == TaskFilters::All)
                  .insert_bool("isMyTasks", s.selected_task_filter == TaskFilters::Mine)
                  .insert_bool("isMyContrib", s.selected_task_filter == TaskFilters::Contrib)
                  .insert_bool("isOpen", s.filter_opened)
                  .insert_bool("loading", false);
               data
           }
           States::Error(s) => {
               let data = mustache::MapBuilder::new().insert("state", &s).unwrap()
                  .insert_bool("isAllTasks", s.selected_task_filter == TaskFilters::All)
                  .insert_bool("isMyTasks", s.selected_task_filter == TaskFilters::Mine)
                  .insert_bool("isMyContrib", s.selected_task_filter == TaskFilters::Contrib)
                  .insert_bool("isOpen", s.filter_opened)
                  .insert_bool("loading", false);
               data
           }
       }
   }
}

use crate::components::task_manager::state::Task;
use wasm_component::v2::driver::dom::AppEvent;
use wasm_component::v2::driver::dom::{ log, error };

use wasm_bindgen::JsCast;
use wasm_bindgen::JsValue;
use wasm_bindgen_futures::JsFuture;

use web_sys::{Request, RequestInit, RequestMode, Response};

fn send_error(name: String, data: String) {
   let window = web_sys::window().unwrap();
   let app_event: AppEvent = AppEvent { 
       id: String::from("fetch-service"), 
       event: format!(r#"{{ "{}": {:?} }}"#, name, data),
   };
   // log(format!("{:?}", app_event).as_str());
   let event = web_sys::CustomEvent::new("app_message").unwrap();
   event.init_custom_event_with_can_bubble_and_cancelable_and_detail(
       format!("App_app_message").as_str(),
       false,
       true,
       &JsValue::from_serde(&app_event).unwrap(),
   );
   match window.dispatch_event(&event) {
       Err(e) => {
           error(format!("could not send event: {:?}", e).as_str());
       }
       _ => {}
   };
}

fn send_success(name: String, data: String) {
   let window = web_sys::window().unwrap();
   let app_event: AppEvent = AppEvent { 
       id: String::from("fetch-service"), 
       event: format!(r#"{{ "{}": {} }}"#, name, data),
   };
   // log(format!("{:?}", app_event).as_str());
   //
   let event = web_sys::CustomEvent::new("app_message").unwrap();
   event.init_custom_event_with_can_bubble_and_cancelable_and_detail(
       format!("App_app_message").as_str(),
       false,
       true,
       &JsValue::from_serde(&app_event).unwrap(),
   );
   match window.dispatch_event(&event) {
       Err(e) => {
           error(format!("could not send event: {:?}", e).as_str());
       }
       _ => {}
   };
}

fn build_request(method: &str, uri: &str, body: Option<JsValue>) -> Request {
   let mut opts = RequestInit::new();
      opts.method(method);
      opts.credentials(web_sys::RequestCredentials::Include);
      opts.mode(RequestMode::Cors);
   
   if body.is_some() {
      opts.body(body.as_ref());
   }

   let host: &str = dotenv!("HOST");
   let url = format!("{}{}", host, uri);

   let request = Request::new_with_str_and_init(&url, &opts)
      .unwrap();

   request
       .headers()
       .set("Accept", "application/json")
       .unwrap();
   request
       .headers()
       .set("Access-Control-Allow-Origin", "*")
       .unwrap();
   request
      .headers()
      .set("Content-Type", "application/json")
      .unwrap();

   request
}

pub async fn get_tasks() {
   let request = build_request(
      "GET",
      "/tasks",
      None,
   );
   let window = web_sys::window().unwrap();
   let resp_value: Option<JsValue> = match JsFuture::from(window.fetch_with_request(&request)).await {
      Ok(v) => Some(v),
      Err(_e) => {
         send_error(
            String::from("GetTasksError"),
            String::from("Could not perform request. Please see console for more errors"),
         );
         None
      }
   };

   match resp_value {
      Some(resp_value) => {
         let resp: Response = resp_value.dyn_into()
             .unwrap();

         let promise = resp.json()
             .unwrap();

         // Convert this other `Promise` into a rust `Future`.
         let json = JsFuture::from(promise).await
             .unwrap();

         // Use serde to parse the JSON into a struct.
         let tasks: Vec<Task> = json.into_serde().unwrap();
         let tasks: serde_json::Value = serde_json::to_value(tasks).unwrap();
         let tasks: String = tasks.to_string(); 

         send_success(
            String::from("GetTasks"),
            tasks,
         );
      },
      None => {}
   };
}

pub async fn get_my_tasks() {
   let request = build_request(
      "GET",
      "/tasks?author_id=Ciro DE CARO",
      None,
   );
   let window = web_sys::window().unwrap();
   let resp_value: Option<JsValue> = match JsFuture::from(window.fetch_with_request(&request)).await {
      Ok(v) => Some(v),
      Err(_e) => {
         send_error(
            String::from("GetTasksError"),
            String::from("Could not perform request. Please see console for more errors"),
         );
         None
      }
   };

   match resp_value {
      Some(resp_value) => {
         let resp: Response = resp_value.dyn_into()
             .unwrap();

         let promise = resp.json()
             .unwrap();

         // Convert this other `Promise` into a rust `Future`.
         let json = JsFuture::from(promise).await
             .unwrap();

         // Use serde to parse the JSON into a struct.
         let tasks: Vec<Task> = json.into_serde().unwrap();
         let tasks: serde_json::Value = serde_json::to_value(tasks).unwrap();
         let tasks: String = tasks.to_string(); 

         send_success(
            String::from("GetTasks"),
            tasks,
         );
      },
      None => {}
   };
}

pub async fn get_my_contrib() {
   let request = build_request(
      "GET",
      "/tasks?contributor_id=Ciro DE CARO",
      None,
   );
   let window = web_sys::window().unwrap();
   let resp_value: Option<JsValue> = match JsFuture::from(window.fetch_with_request(&request)).await {
      Ok(v) => Some(v),
      Err(_e) => {
         send_error(
            String::from("GetTasksError"),
            String::from("Could not perform request. Please see console for more errors"),
         );
         None
      }
   };

   match resp_value {
      Some(resp_value) => {
         let resp: Response = resp_value.dyn_into()
             .unwrap();

         let promise = resp.json()
             .unwrap();

         // Convert this other `Promise` into a rust `Future`.
         let json = JsFuture::from(promise).await
             .unwrap();

         // Use serde to parse the JSON into a struct.
         let tasks: Vec<Task> = json.into_serde().unwrap();
         let tasks: serde_json::Value = serde_json::to_value(tasks).unwrap();
         let tasks: String = tasks.to_string(); 

         send_success(
            String::from("GetTasks"),
            tasks,
         );
      },
      None => {}
   };
}

pub async fn delete_task(id: i32) {
   let request = build_request(
      "DELETE",
      format!("/tasks/{}", id).as_str(),
      None,
   );
   let window = web_sys::window().unwrap();
   let resp_value: Option<JsValue> = match JsFuture::from(window.fetch_with_request(&request)).await {
      Ok(v) => Some(v),
      Err(_e) => {
         send_error(
            String::from("DeleteTaskError"),
            String::from("Could not perform request. Please see console for more errors"),
         );
         None
      }
   };

   match resp_value {
      Some(_) => {
         // log(format!("{:?}", tasks).as_str());
         send_success(
            String::from("DeleteTask"),
            format!(r#""{}""#, id),
         );
      },
      None => {}
   };
}

pub async fn update_task(task: Task) {
   let body_value: serde_json::Value = serde_json::to_value(&task).unwrap();
   let body_value = body_value.to_string();
   let body_value: JsValue = JsValue::from_str(&body_value);

   let request = build_request(
      "PUT",
      format!("/tasks/{}", task.id).as_str(),
      Some(body_value),
   );
   let window = web_sys::window().unwrap();
   let resp_value: Option<JsValue> = match JsFuture::from(window.fetch_with_request(&request)).await {
      Ok(v) => Some(v),
      Err(_e) => {
         send_error(
            String::from("UpdateTaskError"),
            String::from("Could not perform request. Please see console for more errors"),
         );
         None
      }
   };

   match resp_value {
      Some(_) => {
         send_success(
            String::from("UpdateTask"),
            format!(r#""{}""#, task.id),
         );
      },
      None => {}
   };
}

use std::error::Error;
use std::hash::Hash;
use std::cmp::Eq;
use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Failures {
    Message(String),
}

impl std::fmt::Display for Failures {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Failures::Message(s) => write!(f, "State Failure: {}", s)
        }
    }
}

// This is important for other errors to wrap this one.
impl Error for Failures {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        // Generic error, underlying cause isn't tracked.
        None
    }
}

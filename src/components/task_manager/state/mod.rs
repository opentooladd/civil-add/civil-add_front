use std::hash::Hash;
use std::cmp::Eq;
use serde::{ Serialize, Deserialize };

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq)]
pub enum TaskStatus {
   Brouillon,
   Publiee,
   Realisee,
   Archive,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq)]
pub struct Task {
   pub id: i32,
   pub title: String,
   pub body: String,
   pub status: TaskStatus,
   pub author_id: String,
   pub contributor_id: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq)]
pub struct State {
    pub tasks: Vec<Task>,
    pub selected_task_filter: TaskFilters,
    pub filter_opened: bool,
    pub error: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq)]
pub enum TaskFilters {
   All,
   Mine,
   Contrib,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq)]
pub enum States {
    Loading(State),
    Ready(State),
    Error(State),
}

impl Default for States {
    fn default() -> Self {
        States::Ready(State {
           tasks: vec![],
           selected_task_filter: TaskFilters::All,
           filter_opened: false,
           error: None
        })
    }
}

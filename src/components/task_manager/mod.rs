pub mod state;
pub mod machine;
mod events;
mod failures;

use wasm_component::mustache;
use wasm_component::web_sys;
use wasm_component::v2::driver::dom::{ error, log };
use wasm_component::v2::driver::dom::AppEvent;
use wasm_component::v2::driver::dom::Driver;
use wasm_component::v2::driver::dom::component::DOMComponent;
use wasm_component::state_machine::Machine;

pub use machine::TaskManager as TaskManager;

pub enum Components {
   Void,
}

impl DOMComponent for Components {
   fn create() -> Self {
      Components::Void
   }

   fn get_data(&mut self) -> mustache::MapBuilder {
      match self {
         _ => {
            unimplemented!()
         }
      }
   }

   fn get_childrens(&mut self) -> mustache::MapBuilder {
      match self {
         _ => {
            unimplemented!()
         }
      }
   }

   fn view(&self) -> &'static str {
      match self {
         _ => {
            unimplemented!()
         }
      }
   }
   fn style(&self) -> &'static str {
      match self {
         _ => {
            unimplemented!()
         }
      }
   }

   fn receive(&mut self, evt: AppEvent) -> Result<(), String> {
      match self {
         _ => unimplemented!(),
      }
   }

   fn render_child(&mut self, id_element: String, tag: String, parent: Option<web_sys::Element>) -> Result<(), ()> {
      // log("Components: render child");
      match tag.as_str() {
         t => {
            error(format!("no component named {:?} in {:?}", t, "TaskManager").as_str());
            Err(())
         },
      }
   }
}

pub const HTML_TEMPLATE: &'static str = include_str!("./index.html");
pub const CSS_TEMPLATE: &'static str = include_str!("./index.css");

impl DOMComponent for machine::TaskManager {
   fn create() -> Self where Self: DOMComponent {
      machine::TaskManager::new()
   }

   fn get_data(&mut self) -> mustache::MapBuilder {
      Machine::get_data(self)
   }

   fn get_childrens(&mut self) -> mustache::MapBuilder {
      Machine::get_childrens(self)
   }

   fn view(&self) -> &'static str {
      HTML_TEMPLATE
   }
   fn style(&self) -> &'static str {
      CSS_TEMPLATE
   }

   fn receive(&mut self, evt: AppEvent) -> Result<(), String> {
      let c_evt = evt.clone();
      // log(format!("Task Manager DOMComponent: receive: {:?}", c_evt).as_str());
      match Machine::receive(self, c_evt) {
         Ok(_) => {
            Ok(())
         },
         Err(e) => {
            if e.contains("failed transition") {
               // error(format!("TaskManager: receive error: {:?}", e).as_str());
               Err(e)
            } else {
               self.components.values_mut()
                  .filter(|c| c.is_owner_of_event(evt.clone()))
                  .map(|c| c.receive(evt.clone())).collect()
            }
         }
      }
   }

   fn render_child(&mut self, id_element: String, tag: String, parent: Option<web_sys::Element>) -> Result<(), ()> {
      log("TaskManager: render child");
      match tag.as_str() {
         t => {
            error(format!("no component named {:?} in {:?}", t, "TaskManager").as_str());
            Err(())
         },
      }
   }

   fn destroy_child(&mut self, id_element: String) {
      match self.components.remove(&id_element) {
         Some(driver) => { 
            // log(format!("Component removed: {}", id_element).as_str());
            driver.drop();
         },
         None => { log(format!("Component not removed: {}", id_element).as_str()) },
      }
   }
}

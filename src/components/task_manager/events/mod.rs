use serde::{ Serialize, Deserialize };

use crate::components::task_manager::state::{ Task, TaskFilters };

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Eq, Hash)]
pub enum Events {
   FetchTasks,
   GetTasks(Vec<Task>),
   GetTasksError(String),
   // GetTask(String),
   FetchMyTasks,
   FetchMyContrib,
   RedirectToCreateTask,
   RedirectToUpdateTask(i32),
   SendUpdateTask(i32),
   UpdateTask(i32),
   UpdateTaskError(String),
   SendDeleteTask(i32),
   DeleteTask(i32),
   DeleteTaskError(String),
   SelectFilter(TaskFilters),
   OpenFilter,
   CloseFilter,
}

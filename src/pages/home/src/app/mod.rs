use wasm_component::mustache;
use wasm_component::web_sys;
use wasm_component::v2::driver::dom::{ error, log };
use wasm_component::v2::driver::dom::AppEvent;
use wasm_component::v2::driver::dom::Driver;
use wasm_component::v2::driver::dom::component::DOMComponent;
use wasm_component::state_machine::Machine;

pub mod state;
pub mod failures;
pub mod events;
pub mod machine;

pub use machine::App as App;

use civil_add_front::components::task_manager;

pub enum Components {
   Void,
   TaskManager(task_manager::TaskManager),
}

impl DOMComponent for Components {

   fn create() -> Self {
      Components::Void
   }

   fn get_data(&mut self) -> mustache::MapBuilder {
      match self {
         Components::TaskManager(machine) => Machine::get_data(machine),
         _ => {
            unimplemented!()
         }
      }
   }

   fn get_childrens(&mut self) -> mustache::MapBuilder {
      match self {
         Components::TaskManager(machine) => Machine::get_childrens(machine),
         _ => {
            unimplemented!()
         }
      }
   }

   fn view(&self) -> &'static str {
      match self {
         Components::TaskManager(_) => task_manager::HTML_TEMPLATE,
         _ => {
            unimplemented!()
         }
      }
   }
   fn style(&self) -> &'static str {
      match self {
         Components::TaskManager(_) => task_manager::CSS_TEMPLATE,
         _ => {
            unimplemented!()
         }
      }
   }

   fn receive(&mut self, evt: AppEvent) -> Result<(), String> {
      match self {
         Components::TaskManager(machine) => DOMComponent::receive(machine, evt),
         _ => unimplemented!(),
      }
   }

   fn render_child(&mut self, id_element: String, tag: String, parent: Option<web_sys::Element>) -> Result<(), ()> {
      // log("Components: render child");
      match tag.as_str() {
         t => {
            error(format!("no component named {:?} in {:?}", t, "App").as_str());
            Err(())
         },
      }
   }
}

pub const HTML_TEMPLATE: &'static str = include_str!("./index.html");
pub const CSS_TEMPLATE: &'static str = include_str!("./index.css");

impl DOMComponent for machine::App {
   fn create() -> Self {
      machine::App::new()
   }

   fn get_data(&mut self) -> mustache::MapBuilder {
      Machine::get_data(self)
   }

   fn get_childrens(&mut self) -> mustache::MapBuilder {
      Machine::get_childrens(self)
   }

   fn view(&self) -> &'static str {
      HTML_TEMPLATE
   }
   fn style(&self) -> &'static str {
      CSS_TEMPLATE
   }

   fn receive(&mut self, evt: AppEvent) -> Result<(), String> {
      let c_evt = evt.clone();
      // if it is fetch-service, redirect on each child by driver.id
      if evt.id == "fetch-service".to_string() {
         self.components.values_mut().map(|c| {
            let mut n_evt = evt.clone();
            n_evt.id = c.id.clone();
            c.receive(n_evt)
         }).collect()
      } else {
         match Machine::receive(self, c_evt) {
            Ok(_) => {
               Ok(())
            },
            Err(e) => {
               if e.contains("failed transition") {
                  error(format!("App: receive error: {:?}", e).as_str());
                  Err(e)
               } else {
                  self.components.values_mut()
                     .filter(|c| c.is_owner_of_event(evt.clone()))
                     .map(|c| c.receive(evt.clone())).collect()
               }
            }
         }
      } 
   }

   fn render_child(&mut self, id_element: String, tag: String, parent: Option<web_sys::Element>) -> Result<(), ()> {
      match tag.as_str() {
         "TaskManager" => {
            match self.components.get_mut(&id_element) {
               Some(driver) => {
                  driver.render(None);
                  Ok(())
               },
               None => {
                  let mut driver = Driver::new_with_component(Components::TaskManager(task_manager::TaskManager::create()), false);

                  DOMComponent::receive(&mut driver.component, AppEvent {
                     id: String::from("fetch-service"),
                     event: r#"{ "FetchTasks": null }"#.to_string(),
                  }).expect("could not transition");

                  driver.mount(parent.map(|e| e.into()), false);

                  self.components.insert(
                     id_element.clone(),
                     driver,
                  ); 
                  Ok(())
               }
            }
         },
         t => {
            error(format!("no component named {:?} in {:?}", t, "App").as_str());
            Err(())
         },
      }
   }

   fn destroy_child(&mut self, id_element: String) {
      match self.components.remove(&id_element) {
         Some(driver) => { 
            // log(format!("Component removed: {}", id_element).as_str());
            driver.drop();
         },
         None => { log(format!("Component not removed: {}", id_element).as_str()) },
      }
   }
}

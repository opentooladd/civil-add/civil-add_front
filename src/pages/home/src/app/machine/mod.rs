use std::collections::HashMap;

use serde::{ Serialize, Deserialize };

use wasm_component::mustache;
use wasm_component::state_machine::Machine;
use crate::app::state::{
   State,
   States,
};
use crate::app::events::Events;
use crate::app::failures::Failures;
use wasm_component::v2::driver::dom::{ log };
use wasm_component::v2::driver::dom::Driver;

use crate::app::Components;

pub struct App {
    state: States,
    pub components: HashMap<String, Driver<Components>>,
}

impl Machine for App {
   type Events = Events;
   type State = State;
   type States = States;
   type Failures = Failures;

   fn new() -> Self {
        let state = States::default();
        App {
            state: state.clone(),
            components: HashMap::new(),
        }
    }

   fn transition(&mut self, event: Events) -> Result<&States, Failures> {
       // match (self.get_state(), event) {
       // }
       self.run()
   }  
   
   fn run(&mut self) -> Result<&States, Failures> {
       // match self.state.clone() {
       //     
       // }; 
       // log(format!("{:?}", self.get_state()).as_ref());
       Ok(self.get_state())
   }

   fn get_state (&self) -> &States { 
      &self.state
   }

   fn get_state_mut(&mut self) -> &mut States {
       &mut self.state
   } 
    
   fn get_raw_state(&self) -> &State {
        match self.state {
           States::Loading(ref c) | States::Ready(ref c) => c
        }
   }

   fn get_raw_state_mut(&mut self) -> &mut State {
        match self.state {
            States::Loading(ref mut c) | States::Ready(ref mut c) => c
        }
   }

   // optional but permit to use more functionnality of mustache
   // like lambdas and to react to other combination
   fn get_data(&mut self) -> mustache::MapBuilder {
       match self.state.clone() {
           States::Loading(s) => {
               let data = mustache::MapBuilder::new().insert("state", &s).unwrap().insert_bool("loading", true);
               data
           }
           States::Ready(s) => {
               let data = mustache::MapBuilder::new().insert("state", &s).unwrap().insert_bool("loading", false);
               data
           }
       }
   }
}

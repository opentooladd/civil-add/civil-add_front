use std::hash::Hash;
use std::cmp::Eq;
use serde::{ Serialize, Deserialize };

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq, Default)]
pub struct Task {
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq, Default)]
pub struct State {
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, Hash, Eq)]
pub enum States {
    Loading(State),
    Ready(State),
}

impl Default for States {
    fn default() -> Self {
        States::Ready(State { })
    }
}

# wasm-app-boilerplate

The boilerplate project for [wasm-boilerplate](https://gitlab.com/opentooladd/wasm-boilerplate).

## Tests

+ Unit tests: [Jest](https://jestjs.io/docs/en/getting-started.html)
+ E2E tests: [TestCafe](https://devexpress.github.io/testcafe/documentation/test-api/)

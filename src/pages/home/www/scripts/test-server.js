#!/bin/env node

const path = require('path');
const express = require('express');
const app = express();

// Set the MIME type explicitly
express.static.mime.define({'application/wasm': ['wasm']});

app.use(express.static(path.resolve(process.cwd(), './dist')));

app.listen(8080);

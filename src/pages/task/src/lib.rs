extern crate civil_add_front;
extern crate futures;
extern crate web_sys;
extern crate wasm_component;
extern crate serde;
extern crate serde_derive;
extern crate serde_json;

mod app;
mod utils;

use wasm_component::wasm_bindgen;
use wasm_component::wasm_bindgen::prelude::*;

use wasm_component::v2::app::dom::{
   App,
};

#[wasm_bindgen(start)]
pub fn main() {
   std::panic::set_hook(Box::new(console_error_panic_hook::hook));
   let app = App::<app::App>::new(true);

   app.mount_to_body().forget();
}

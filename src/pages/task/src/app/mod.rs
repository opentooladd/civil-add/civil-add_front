use wasm_component::mustache;
use wasm_component::web_sys;
use wasm_component::v2::driver::dom::{ error, log };
use wasm_component::v2::driver::dom::AppEvent;
use wasm_component::v2::driver::dom::Driver;
use wasm_component::v2::driver::dom::component::DOMComponent;
use wasm_component::state_machine::Machine;

pub mod state;
pub mod failures;
pub mod events;
pub mod machine;

pub use machine::App as App;

use civil_add_front::components::task_create;
use civil_add_front::components::task_update;

pub enum Components {
   Void,
   TaskCreate(task_create::TaskCreate),
   TaskUpdate(task_update::TaskUpdate),
}

impl DOMComponent for Components {
   fn create() -> Self {
      Components::Void
   }

   fn get_data(&mut self) -> mustache::MapBuilder {
      match self {
         Components::TaskCreate(machine) => Machine::get_data(machine),
         Components::TaskUpdate(machine) => Machine::get_data(machine),
         _ => {
            unimplemented!()
         }
      }
   }

   fn get_childrens(&mut self) -> mustache::MapBuilder {
      match self {
         Components::TaskCreate(machine) => Machine::get_childrens(machine),
         Components::TaskUpdate(machine) => Machine::get_childrens(machine),
         _ => {
            unimplemented!()
         }
      }
   }

   fn view(&self) -> &'static str {
      match self {
         Components::TaskCreate(_) => task_create::HTML_TEMPLATE,
         Components::TaskUpdate(_) => task_update::HTML_TEMPLATE,
         _ => {
            unimplemented!()
         }
      }
   }
   fn style(&self) -> &'static str {
      match self {
         Components::TaskCreate(_) => task_create::CSS_TEMPLATE,
         Components::TaskUpdate(_) => task_update::CSS_TEMPLATE,
         _ => {
            unimplemented!()
         }
      }
   }

   fn receive(&mut self, evt: AppEvent) -> Result<(), String> {
      match self {
         Components::TaskCreate(machine) => DOMComponent::receive(machine, evt),
         Components::TaskUpdate(machine) => DOMComponent::receive(machine, evt),
         _ => unimplemented!(),
      }
   }

   fn render_child(&mut self, id_element: String, tag: String, parent: Option<web_sys::Element>) -> Result<(), ()> {
      // log("Components: render child");
      match tag.as_str() {
         t => {
            error(format!("no component named {:?} in {:?}", t, "App").as_str());
            Err(())
         },
      }
   }
}

pub const HTML_TEMPLATE: &'static str = include_str!("./index.html");
pub const CSS_TEMPLATE: &'static str = include_str!("./index.css");

impl DOMComponent for machine::App {
   fn create() -> Self {
      machine::App::new()
   }

   fn get_data(&mut self) -> mustache::MapBuilder {
      Machine::get_data(self)
   }

   fn get_childrens(&mut self) -> mustache::MapBuilder {
      Machine::get_childrens(self)
   }

   fn view(&self) -> &'static str {
      HTML_TEMPLATE
   }
   fn style(&self) -> &'static str {
      CSS_TEMPLATE
   }

   fn receive(&mut self, evt: AppEvent) -> Result<(), String> {
      let c_evt = evt.clone();
      // if it is fetch-service, redirect on each child by driver.id
      if evt.id == "fetch-service".to_string() {
         self.components.values_mut().map(|c| {
            let mut n_evt = evt.clone();
            n_evt.id = c.id.clone();
            c.receive(n_evt)
         }).collect()
      } else {
         match Machine::receive(self, c_evt) {
            Ok(_) => {
               Ok(())
            },
            Err(e) => {
               if e.contains("failed transition") {
                  error(format!("App: receive error: {:?}", e).as_str());
                  Err(e)
               } else {
                  self.components.values_mut()
                     .filter(|c| c.is_owner_of_event(evt.clone()))
                     .map(|c| c.receive(evt.clone())).collect()
               }
            }
         }
      } 
   }

   fn render_child(&mut self, id_element: String, tag: String, parent: Option<web_sys::Element>) -> Result<(), ()> {
      match tag.as_str() {
         "CreateOrUpdateForm" => {
            match self.components.get_mut(&id_element) {
               Some(driver) => {
                  driver.render(None);
                  Ok(())
               },
               None => {
                  let window = web_sys::window().expect("could not get window");
                  let search_params: web_sys::UrlSearchParams = web_sys::UrlSearchParams::new_with_str(window.location().search().unwrap().replace("?", "").as_str()).unwrap();
                  log(format!("{:?}", search_params.get("id")).as_str());

                  match search_params.get("id") {
                     Some(id) => {
                        let mut driver = Driver::new_with_component(Components::TaskUpdate(task_update::TaskUpdate::create()), false);

                        DOMComponent::receive(&mut driver.component, AppEvent {
                           id: String::from("fetch-service"),
                           event: format!(r#"{{ "FetchTask": {} }}"#, id),
                        }).expect("could not transition");

                        driver.mount(parent.map(|e| e.into()), false);

                        self.components.insert(
                           id_element.clone(),
                           driver,
                        ); 
                        Ok(())
                     }
                     None => { 
                        let mut driver = Driver::new_with_component(Components::TaskCreate(task_create::TaskCreate::create()), false);

                        driver.mount(parent.map(|e| e.into()), false);

                        self.components.insert(
                           id_element.clone(),
                           driver,
                        ); 
                        Ok(())
                     }
                  } 
               }
            }
         },
         t => {
            error(format!("no component named {:?} in {:?}", t, "App").as_str());
            Err(())
         },
      }
   }

   fn destroy_child(&mut self, id_element: String) {
      match self.components.remove(&id_element) {
         Some(driver) => { 
            // log(format!("Component removed: {}", id_element).as_str());
            driver.drop();
         },
         None => { log(format!("Component not removed: {}", id_element).as_str()) },
      }
   }
}

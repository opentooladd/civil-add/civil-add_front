#!/bin/bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SOURCE_FOLDER=$THIS_DIR/../src
FILES=$(find $SOURCE_FOLDER -name '*.scss')

# copy pattern-lab css to public
# for the moment it is hard-coded
# but in the end the pattern-lab will be a package
# and the css could be CDN hosted
cp /home/ciro/TOOL-ADD/open_tool-add/resources/pattern-lab/source/css/style.css $SOURCE_FOLDER/css/pattern-lab.scss

for f in $FILES
do
  node-sass $f ${f/scss/css}
done

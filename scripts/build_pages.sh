#!/bin/bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
OUT_FOLDER=$THIS_DIR/../target/web
SOURCE_FOLDER=$THIS_DIR/../src
PAGES=$SOURCE_FOLDER/pages/*

rm -rf $OUT_FOLDER
mkdir -p $OUT_FOLDER

for f in $PAGES
do
  echo "Launching build for: $f"
  PAGE_NAME=$(basename $f)
  cd $f && make && cd www && npm run build
  cp -a $f/www/dist/. $OUT_FOLDER/$PAGE_NAME
done
